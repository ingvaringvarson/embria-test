
$('.js-filter-link').on('click', function () {
    $(this).parent().children().removeClass('filter__link_active');
    $(this).addClass('filter__link_active');
});

$(function () {
    $('.js-filter-link').on('click', function () {
        let selectedClass = $(this).attr('data-rel');
        $('#filterList').stop(true, true).fadeTo(100, 0);
        $('#filterList .js-filter-item').stop().fadeOut().removeClass('animation-scale');
        setTimeout(function () {
            $('.' + selectedClass).stop().fadeIn().addClass('animation-scale');
            $('#filterList').stop(true, true).fadeTo(400, 1);
        }, 400);
    });
});
