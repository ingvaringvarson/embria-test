'use strict';

import polyfills from './libraries/polyfills';

import 'jquery';
import 'svg4everybody';

import 'components/tournaments/tournaments';
import 'components/filter/filter';
import 'components/scroll-top/scroll-top';

$(() => {
    polyfills.init();
});
